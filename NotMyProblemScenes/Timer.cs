using Godot;
using System;

public class Timer : Godot.Timer
{
	[Export]
	string change = "title.tscn";
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}
	
	private void _on_Timer_timeout()
	{
		this.GetTree().ChangeScene(change);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
