using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;

public class SmalltalkVM : Node
{

	public MemoryMappedFile mmf;                                            // Shared memory variables, map and view
	public MemoryMappedViewAccessor accessor;

	public Dictionary<uint, Opcode> opcodes = new Dictionary<uint, Opcode>();                             // Opcode table, register table, symbol table, event table, disassembly table
	public Hashtable globalRegisters = new Hashtable();
	public Hashtable symbolTable = new Hashtable();
	public Hashtable events = new Hashtable();
	public Dictionary<uint, string> disassembly = new Dictionary<uint, string>();
	public int scanner = 0;                                                 // Current byte the VM is scanning
	public ArrayList globalRoutine = new ArrayList() { };
	public ArrayList oldRoutine = new ArrayList() { };
	public ArrayList eventRoutine = new ArrayList() { };
	public ArrayList oldEventRoutine = new ArrayList() { };
	public int readingCounter = 0;                                              // To keep track of nested reads
	public bool routineLoop = false;                                        // Loop the remaining bytes in routine bytearray
	[Export] public string memoryAddress = "Smalltalk";                 // The name of the memory address to connect to if not interpreting
	[Export] public bool export = false;                                    // End-user options, export bytecode to file,
	[Export] public bool interpret = false;                                 // read bytecode from file
	[Export] public bool debug = false;                                     // For debugging the VM, lots of console spam
	[Export] public bool disassemble = false;
	[Export(PropertyHint.File, "*.stb")] public string bytecodeFile;        // Browse to a Smalltalk Bytecode file for interpreting. Unneccessary if interpret unchecked
	public float deltaTime = 0.0f;                                          // Delta time global
	public Vector3 upDir = new Vector3(0, 1, 0);
	public float mouseRelX = 0;
	public float mouseRelY = 0;
	public uint stackTop = 0;
	public string runningString = "";
	public ArrayList runningOpcodes = new ArrayList();
	public ArrayList runningProcess = new ArrayList();
	public int difference = 4;
	public uint conditionKey = 0;
	public Stopwatch effTest = new Stopwatch();


	// Opcode identities
	public const uint OPCODE_NULL = 0;
	public const uint OPCODE_PUSHI = 1; // IMMEDIATE
	public const uint OPCODE_PUSHF = 2; // IMMEDIATE
	public const uint OPCODE_PUSHB = 3; // IMMEDIATE
	public const uint OPCODE_PUSHSTR = 4; // AFTER READ
	public const uint OPCODE_PUSHPROC = 5; // AFTER READ
	public const uint OPCODE_PULLI = 6; // IMMEDIATE
	public const uint OPCODE_READBEGIN = 7; // META
	public const uint OPCODE_READEND = 8; // META
	public const uint OPCODE_ASSIGN = 9;
	public const uint OPCODE_CONDBEGIN = 10; // IMMEDIATE
	public const uint OPCODE_CONDEND = 11; // IMMEDIATE
	public const uint OPCODE_CONDSET = 12;
	public const uint OPCODE_EOF = 13;
	public const uint OPCODE_ADD = 14;
	public const uint OPCODE_SUB = 15;
	public const uint OPCODE_MUL = 16;
	public const uint OPCODE_DIV = 17;
	public const uint OPCODE_NEGATE = 18;
	public const uint OPCODE_PRINT = 50;
	public const uint OPCODE_MOVESLIDE = 51;
	public const uint OPCODE_GETNODE = 52;
	public const uint OPCODE_DELTATIME = 53;
	public const uint OPCODE_ISPRESSED = 54;
	public const uint OPCODE_SETVEC3X = 56;
	public const uint OPCODE_SETVEC3Y = 57;
	public const uint OPCODE_SETVEC3Z = 58;
	public const uint OPCODE_SETVEC3XYZ = 59;
	public const uint OPCODE_PUSHVEC3X = 60;
	public const uint OPCODE_PUSHVEC3Y = 61;
	public const uint OPCODE_PUSHVEC3Z = 62;
	public const uint OPCODE_SETVEC2X = 63;
	public const uint OPCODE_SETVEC2Y = 64;
	public const uint OPCODE_SETVEC2XY = 65;
	public const uint OPCODE_PUSHVEC2X = 66;
	public const uint OPCODE_PUSHVEC2Y = 67;
	public const uint OPCODE_NORMALIZE = 68;
	public const uint OPCODE_GLOBALXFORM = 69;
	public const uint OPCODE_XFORMBASISX = 70;
	public const uint OPCODE_XFORMBASISY = 71;
	public const uint OPCODE_XFORMBASISZ = 72;
	public const uint OPCODE_DOT = 73;
	public const uint OPCODE_LERP = 74;
	public const uint OPCODE_MOVESLIDERESULT = 75;
	public const uint OPCODE_GRTHAN = 76;
	public const uint OPCODE_GRTHANEQ = 77;
	public const uint OPCODE_LSTHAN = 78;
	public const uint OPCODE_LSTHANEQ = 79;
	public const uint OPCODE_EQUAL = 80;
	public const uint OPCODE_NOTEQUAL = 81;
	public const uint OPCODE_ROTATEX = 82;
	public const uint OPCODE_ROTATEY = 83;
	public const uint OPCODE_ROTATEZ = 84;
	public const uint OPCODE_DEG2RAD = 85;
	public const uint OPCODE_CLAMP = 86;
	public const uint OPCODE_STR2EVENT = 87;
	public const uint OPCODE_MOUSERELX = 88;
	public const uint OPCODE_MOUSERELY = 89;
	public const uint OPCODE_MOUSECAPTURED = 90;
	public const uint OPCODE_MOUSEVISIBLE = 91;
	public const uint OPCODE_MOUSEHIDDEN = 92;
	public const uint OPCODE_MOUSECONFINED = 93;
	public const uint OPCODE_ISONFLOOR = 94;
	public const uint OPCODE_CSINVOKE0 = 95;
	public const uint OPCODE_CSINVOKE1 = 96;
	public const uint OPCODE_CSINVOKE2 = 97;
	public const uint OPCODE_CSINVOKE3 = 98;
	public const uint OPCODE_CSINVOKE4 = 99;
	public const uint OPCODE_CSINVOKE5 = 100;
	public const uint OPCODE_POSX = 101;
	public const uint OPCODE_POSY = 102;
	public const uint OPCODE_POSZ = 103;
	public const uint OPCODE_POS = 104;
	public const uint OPCODE_SETPOS = 105;
	public const uint OPCODE_SETPOSX = 106;
	public const uint OPCODE_SETPOSY = 107;
	public const uint OPCODE_SETPOSZ = 108;
	public const uint OPCODE_ROTX = 109;
	public const uint OPCODE_ROTY = 110;
	public const uint OPCODE_ROTZ = 111;
	public const uint OPCODE_ROT = 112;
	public const uint OPCODE_SETROT = 113;
	public const uint OPCODE_SETROTX = 114;
	public const uint OPCODE_SETROTY = 115;
	public const uint OPCODE_SETROTZ = 116;
	public const uint OPCODE_PLAYASTREAM = 117;
	public const uint OPCODE_STOPASTREAM = 118;
	public const uint OPCODE_JUSTPRESSED = 119;
	public const uint OPCODE_CHANGESCENE = 120;
	public const uint OPCODE_SETVISIBLE = 121;
	public const uint OPCODE_NOT = 122;
	public const uint OPCODE_SETAVOLUME = 123;
	public const uint OPCODE_SETANIMPARAM = 124;
	public const uint OPCODE_SETANIMPARAMT = 125;
	public const uint OPCODE_SETANIMPARAMF = 126;
	public const uint OPCODE_CURRANIM = 127;
	public const uint OPCODE_GETPLAYBACK = 128;

	/*
		public const uint OPCODE_PLAYANIM = 103;
		public const uint OPCODE_NOT = 104;
		public const uint OPCODE_SETGPOS = 107;
		public const uint OPCODE_SETGROT = 108;
		public const uint OPCODE_GETGPOS = 109;
		public const uint OPCODE_GETGROT = 110;
		public const uint OPCODE_TOSTRING = 111;
		public const uint OPCODE_SETTEXT = 112;
		public const uint OPCODE_TOBLACK = 113;
		public const uint OPCODE_RESETENV = 115;*/

	public bool torn = false;
	public bool broken = false;
	public bool lost = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// If the user hasn't checked "interpret", activate live coding by sharing 2000 bytes of memory for writing
		if (!interpret)
		{
			mmf = MemoryMappedFile.CreateOrOpen(memoryAddress, 64000);

			accessor = mmf.CreateViewAccessor();
		}

		// Binding opcode constants to their classes and execution code


		opcodes.Add(OPCODE_NULL, new Op_Null());
		opcodes.Add(OPCODE_PUSHB, new Op_PushBool());
		opcodes.Add(OPCODE_PUSHI, new Op_PushInteger());
		opcodes.Add(OPCODE_PUSHF, new Op_PushFloat());
		opcodes.Add(OPCODE_PUSHSTR, new Op_PushString());
		opcodes.Add(OPCODE_PUSHPROC, new Op_PushProcess());
		opcodes.Add(OPCODE_PULLI, new Op_PullInteger());
		opcodes.Add(OPCODE_READBEGIN, new Op_Null());
		opcodes.Add(OPCODE_READEND, new Op_Null());
		opcodes.Add(OPCODE_ASSIGN, new Op_AssignSymbol());
		opcodes.Add(OPCODE_CONDBEGIN, new Op_ConditionBegin());
		opcodes.Add(OPCODE_CONDEND, new Op_ConditionEnd());
		opcodes.Add(OPCODE_CONDSET, new Op_SetCondition());
		opcodes.Add(OPCODE_EOF, new Op_Null());
		opcodes.Add(OPCODE_PRINT, new Op_Print());
		opcodes.Add(OPCODE_MOVESLIDE, new Op_MoveAndSlide());
		opcodes.Add(OPCODE_GETNODE, new Op_GetNode());
		opcodes.Add(OPCODE_ADD, new Op_Add());
		opcodes.Add(OPCODE_SUB, new Op_Subtract());
		opcodes.Add(OPCODE_MUL, new Op_Multiply());
		opcodes.Add(OPCODE_DIV, new Op_Divide());
		opcodes.Add(OPCODE_NEGATE, new Op_Negate());
		opcodes.Add(OPCODE_DELTATIME, new Op_DeltaTime());
		opcodes.Add(OPCODE_ISPRESSED, new Op_IsPressed());
		opcodes.Add(OPCODE_SETVEC3X, new Op_SetVector3X());
		opcodes.Add(OPCODE_SETVEC3Y, new Op_SetVector3Y());
		opcodes.Add(OPCODE_SETVEC3Z, new Op_SetVector3Z());
		opcodes.Add(OPCODE_SETVEC3XYZ, new Op_SetVector3XYZ());
		opcodes.Add(OPCODE_PUSHVEC3X, new Op_PushVector3X());
		opcodes.Add(OPCODE_PUSHVEC3Y, new Op_PushVector3Y());
		opcodes.Add(OPCODE_PUSHVEC3Z, new Op_PushVector3Z());
		opcodes.Add(OPCODE_SETVEC2X, new Op_SetVector2X());
		opcodes.Add(OPCODE_SETVEC2Y, new Op_SetVector2Y());
		opcodes.Add(OPCODE_SETVEC2XY, new Op_SetVector2XY());
		opcodes.Add(OPCODE_PUSHVEC2X, new Op_PushVector2X());
		opcodes.Add(OPCODE_PUSHVEC2Y, new Op_PushVector2Y());
		opcodes.Add(OPCODE_NORMALIZE, new Op_NormalizeVector());
		opcodes.Add(OPCODE_GLOBALXFORM, new Op_PushGlobalTransform());
		opcodes.Add(OPCODE_XFORMBASISX, new Op_PushBasisX());
		opcodes.Add(OPCODE_XFORMBASISY, new Op_PushBasisY());
		opcodes.Add(OPCODE_XFORMBASISZ, new Op_PushBasisZ());
		opcodes.Add(OPCODE_DOT, new Op_DotProduct());
		opcodes.Add(OPCODE_LERP, new Op_LinearInterpolate());
		opcodes.Add(OPCODE_MOVESLIDERESULT, new Op_PushMoveSlideResult());
		opcodes.Add(OPCODE_GRTHAN, new Op_GreaterThan());
		opcodes.Add(OPCODE_GRTHANEQ, new Op_GreaterThanEqual());
		opcodes.Add(OPCODE_LSTHAN, new Op_LessThan());
		opcodes.Add(OPCODE_LSTHANEQ, new Op_LessThanEqual());
		opcodes.Add(OPCODE_EQUAL, new Op_Equal());
		opcodes.Add(OPCODE_NOTEQUAL, new Op_NotEqual());
		opcodes.Add(OPCODE_ROTATEX, new Op_RotateX());
		opcodes.Add(OPCODE_ROTATEY, new Op_RotateY());
		opcodes.Add(OPCODE_ROTATEZ, new Op_RotateZ());
		opcodes.Add(OPCODE_DEG2RAD, new Op_DegreesToRadians());
		opcodes.Add(OPCODE_CLAMP, new Op_Clamp());
		opcodes.Add(OPCODE_STR2EVENT, new Op_StringToEvent());
		opcodes.Add(OPCODE_MOUSERELX, new Op_PushMouseRelativeX());
		opcodes.Add(OPCODE_MOUSERELY, new Op_PushMouseRelativeY());
		opcodes.Add(OPCODE_MOUSECAPTURED, new Op_SetMouseModeCaptured());
		opcodes.Add(OPCODE_MOUSEVISIBLE, new Op_SetMouseModeVisible());
		opcodes.Add(OPCODE_MOUSEHIDDEN, new Op_SetMouseModeHidden());
		opcodes.Add(OPCODE_MOUSECONFINED, new Op_SetMouseModeConfined());
		opcodes.Add(OPCODE_ISONFLOOR, new Op_IsOnFloor());
		opcodes.Add(OPCODE_CSINVOKE0, new Op_CSharpInvoke0());
		opcodes.Add(OPCODE_CSINVOKE1, new Op_CSharpInvoke1());
		opcodes.Add(OPCODE_CSINVOKE2, new Op_CSharpInvoke2());
		opcodes.Add(OPCODE_CSINVOKE3, new Op_CSharpInvoke3());
		opcodes.Add(OPCODE_CSINVOKE4, new Op_CSharpInvoke4());
		opcodes.Add(OPCODE_CSINVOKE5, new Op_CSharpInvoke5());
		opcodes.Add(OPCODE_POSX, new Op_PushPositionX());
		opcodes.Add(OPCODE_POSY, new Op_PushPositionY());
		opcodes.Add(OPCODE_POSZ, new Op_PushPositionZ());
		opcodes.Add(OPCODE_POS, new Op_PushPosition());
		opcodes.Add(OPCODE_SETPOS, new Op_SetPosition());
		opcodes.Add(OPCODE_SETPOSX, new Op_SetPositionX());
		opcodes.Add(OPCODE_SETPOSY, new Op_SetPositionY());
		opcodes.Add(OPCODE_SETPOSZ, new Op_SetPositionZ());
		opcodes.Add(OPCODE_ROTX, new Op_PushRotationX());
		opcodes.Add(OPCODE_ROTY, new Op_PushRotationY());
		opcodes.Add(OPCODE_ROTZ, new Op_PushRotationZ());
		opcodes.Add(OPCODE_ROT, new Op_PushRotation());
		opcodes.Add(OPCODE_SETROT, new Op_SetRotation());
		opcodes.Add(OPCODE_SETROTX, new Op_SetRotationX());
		opcodes.Add(OPCODE_SETROTY, new Op_SetRotationY());
		opcodes.Add(OPCODE_SETROTZ, new Op_SetRotationZ());
		opcodes.Add(OPCODE_PLAYASTREAM, new Op_PlayAudioStream());
		opcodes.Add(OPCODE_STOPASTREAM, new Op_StopAudioStream());
		opcodes.Add(OPCODE_JUSTPRESSED, new Op_JustPressed());
		opcodes.Add(OPCODE_CHANGESCENE, new Op_ChangeScene());
		opcodes.Add(OPCODE_SETVISIBLE, new Op_SetVisible());
		opcodes.Add(OPCODE_NOT, new Op_Not());
		opcodes.Add(OPCODE_SETAVOLUME, new Op_SetAudioStreamVolume());
		opcodes.Add(OPCODE_SETANIMPARAM, new Op_SetAnimationTreeParameter());
		opcodes.Add(OPCODE_SETANIMPARAMT, new Op_SetAnimationTreeParameterTrue());
		opcodes.Add(OPCODE_SETANIMPARAMF, new Op_SetAnimationTreeParameterFalse());
		opcodes.Add(OPCODE_CURRANIM, new Op_GetCurrentAnimationPlayer());
		opcodes.Add(OPCODE_GETPLAYBACK, new Op_GetPlayback());

		if (disassemble)
		{
			disassembly.Add(OPCODE_NULL, "OP_NULL");
			disassembly.Add(OPCODE_PUSHB, "OP_PUSHB");
			disassembly.Add(OPCODE_PUSHI, "OP_PUSHI");
			disassembly.Add(OPCODE_PUSHF, "OP_PUSHF");
			disassembly.Add(OPCODE_PUSHSTR, "OP_PUSHSTR");
			disassembly.Add(OPCODE_PUSHPROC, "OP_PUSHPROC");
			disassembly.Add(OPCODE_PULLI, "OP_PULLI");
			disassembly.Add(OPCODE_READBEGIN, "OP_READBEGIN");
			disassembly.Add(OPCODE_READEND, "OP_READEND");
			disassembly.Add(OPCODE_ASSIGN, "OP_ASSIGN");
			disassembly.Add(OPCODE_EOF, "OP_EOF");
			disassembly.Add(OPCODE_PRINT, "OP_PRINT");
			disassembly.Add(OPCODE_MOVESLIDE, "OP_MOVESLIDE");
			disassembly.Add(OPCODE_GETNODE, "OP_GETNODE");
			disassembly.Add(OPCODE_ADD, "OP_ADD");
			disassembly.Add(OPCODE_SUB, "OP_SUB");
			disassembly.Add(OPCODE_MUL, "OP_MUL");
			disassembly.Add(OPCODE_DIV, "OP_DIV");
			disassembly.Add(OPCODE_NEGATE, "OP_NEGATE");
			disassembly.Add(OPCODE_DELTATIME, "OP_DELTATIME");
			disassembly.Add(OPCODE_ISPRESSED, "OP_ISPRESSED");
			disassembly.Add(OPCODE_SETVEC3X, "OP_SETVEC3X");
			disassembly.Add(OPCODE_SETVEC3Y, "OP_SETVEC3Y");
			disassembly.Add(OPCODE_SETVEC3Z, "OP_SETVEC3Z");
			disassembly.Add(OPCODE_SETVEC3XYZ, "OP_SETVEC3XYZ");
			disassembly.Add(OPCODE_SETVEC2X, "OP_SETVEC2X");
			disassembly.Add(OPCODE_SETVEC2Y, "OP_SETVEC2Y");
			disassembly.Add(OPCODE_SETVEC2XY, "OP_SETVEC2XY");
			disassembly.Add(OPCODE_PUSHVEC3X, "OP_PUSHVEC3X");
			disassembly.Add(OPCODE_PUSHVEC3Y, "OP_PUSHVEC3Y");
			disassembly.Add(OPCODE_PUSHVEC3Z, "OP_PUSHVEC3Z");
			disassembly.Add(OPCODE_PUSHVEC2X, "OP_PUSHVEC2X");
			disassembly.Add(OPCODE_PUSHVEC2Y, "OP_PUSHVEC2Y");
			disassembly.Add(OPCODE_NORMALIZE, "OP_NORMALIZE");
			disassembly.Add(OPCODE_DOT, "OP_DOT");
			disassembly.Add(OPCODE_LERP, "OP_LERP");
			disassembly.Add(OPCODE_MOVESLIDERESULT, "OP_MOVESLIDERESULT");
			disassembly.Add(OPCODE_GRTHAN, "OP_GRTHAN");
			disassembly.Add(OPCODE_GRTHANEQ, "OP_GRTHANEQ");
			disassembly.Add(OPCODE_LSTHAN, "OP_LSTHAN");
			disassembly.Add(OPCODE_LSTHANEQ, "OP_LSTHANEQ");
			disassembly.Add(OPCODE_EQUAL, "OP_EQUAL");
			disassembly.Add(OPCODE_NOTEQUAL, "OP_NOTEQUAL");
			disassembly.Add(OPCODE_ROTATEX, "OP_ROTATEX");
			disassembly.Add(OPCODE_ROTATEY, "OP_ROTATEY");
			disassembly.Add(OPCODE_ROTATEZ, "OP_ROTATEZ");
			disassembly.Add(OPCODE_DEG2RAD, "OP_DEG2RAD");
			disassembly.Add(OPCODE_CLAMP, "OP_CLAMP");
			disassembly.Add(OPCODE_STR2EVENT, "OP_STR2EVENT");
			disassembly.Add(OPCODE_MOUSERELX, "OP_MOUSERELX");
			disassembly.Add(OPCODE_MOUSERELY, "OP_MOUSERELY");
			disassembly.Add(OPCODE_MOUSECAPTURED, "OP_MOUSECAPTURED");
			disassembly.Add(OPCODE_MOUSEVISIBLE, "OP_MOUSEVISIBLE");
			disassembly.Add(OPCODE_MOUSEHIDDEN, "OP_MOUSEHIDDEN");
			disassembly.Add(OPCODE_MOUSECONFINED, "OP_MOUSECONFINED");
			disassembly.Add(OPCODE_ISONFLOOR, "OP_ISONFLOOR");
			disassembly.Add(OPCODE_CSINVOKE0, "OP_CSINVOKE0");
			disassembly.Add(OPCODE_CSINVOKE1, "OP_CSINVOKE1");
			disassembly.Add(OPCODE_CSINVOKE2, "OP_CSINVOKE2");
			disassembly.Add(OPCODE_CSINVOKE3, "OP_CSINVOKE3");
			disassembly.Add(OPCODE_CSINVOKE4, "OP_CSINVOKE4");
			disassembly.Add(OPCODE_CSINVOKE5, "OP_CSINVOKE5");
			disassembly.Add(OPCODE_POSX, "OP_POSX");
			disassembly.Add(OPCODE_POSY, "OP_POSY");
			disassembly.Add(OPCODE_POSZ, "OP_POSZ");
			disassembly.Add(OPCODE_POS, "OP_POS");
			disassembly.Add(OPCODE_SETPOS, "OP_SETPOS");
			disassembly.Add(OPCODE_SETPOSX, "OP_SETPOSX");
			disassembly.Add(OPCODE_SETPOSY, "OP_SETPOSY");
			disassembly.Add(OPCODE_SETPOSZ, "OP_SETPOSZ");
			disassembly.Add(OPCODE_ROTX, "OP_ROTX");
			disassembly.Add(OPCODE_ROTY, "OP_ROTY");
			disassembly.Add(OPCODE_ROTZ, "OP_ROTZ");
			disassembly.Add(OPCODE_ROT, "OP_ROT");
			disassembly.Add(OPCODE_SETROT, "OP_SETROT");
			disassembly.Add(OPCODE_SETROTX, "OP_SETROTX");
			disassembly.Add(OPCODE_SETROTY, "OP_SETROTY");
			disassembly.Add(OPCODE_SETROTZ, "OP_SETROTZ");
			disassembly.Add(OPCODE_PLAYASTREAM, "OP_PLAYASTREAM");
			disassembly.Add(OPCODE_STOPASTREAM, "OP_STOPASTREAM");
			disassembly.Add(OPCODE_JUSTPRESSED, "OP_JUSTPRESSED");
			disassembly.Add(OPCODE_CHANGESCENE, "OP_CHANGESCENE");
			disassembly.Add(OPCODE_NOT, "OP_NOT");
			disassembly.Add(OPCODE_CURRANIM, "OP_CURRANIM");
			disassembly.Add(OPCODE_GETPLAYBACK, "OP_GETPLAYBACK");
		}

		globalRegisters["cond"] = true; // Register initialized for holding the result of conditional checks
										// If we're reading from pre-existing bytecode, do that instead of reading from memory (interpret mode)
		if (interpret) Interpret();
	}

	// This function runs only once when the first 4 bytes indicate a place in memory to jump to, which whenever you run
	// VMScanner runGodotVM in Smalltalk, is always 4, starting the byte code instructions from the beginning.
	public void Compute(int scanpoint)
	{
		Godot.File disassemblyFile = new Godot.File();
		if (disassemble) disassemblyFile.Open("res://disassembled.stb", Godot.File.ModeFlags.Write);
		routineLoop = false;                            // Stop looping our current process to prevent threading problems
		var condition = false;                          // If condition is on, only execute the following opcodes if the condition register is true
		var conditionCounter = 0;
		var reading = false;                            // If reading is on, don't execute the bytes at all. Instead read them as ASCII to a string and put it in the hold
		var readingCounter = 0;
		var lastScanpoint = scanpoint;                  // register. Keep track of our last scanned byte because if we skip anything, we still need to record it.
		uint currOp;
		runningProcess = new ArrayList();
		difference = 4;
		var nullReadCounter = 11;
		while (nullReadCounter <= 10 || accessor.ReadByte(scanpoint) != 0)       // End scanning only when we read in a null
		{
			if (accessor.ReadByte(scanpoint) == 0)
			{
				nullReadCounter += 1;
			}
			else
			{
				nullReadCounter = 0;
			}
			currOp = accessor.ReadUInt32(scanpoint);
			if (currOp == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference) == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference * 2) == OPCODE_READBEGIN && readingCounter == 0 && !condition ||
				currOp == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference) == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference * 2) == OPCODE_READBEGIN && readingCounter == 0 && (bool)globalRegisters["cond"] == true)
			{
				if (disassemble) disassemblyFile.StoreLine(disassembly[currOp]);
				reading = true;
				readingCounter += 1;
				runningString = "";
				runningOpcodes = new ArrayList();
				scanpoint += difference * 3;
				currOp = accessor.ReadUInt32(scanpoint);
			}
			if (reading && currOp == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference) == OPCODE_READBEGIN && accessor.ReadUInt32(scanpoint + difference * 2) == OPCODE_READBEGIN)
				readingCounter += 1;
			lastScanpoint = scanpoint;
			if ((!reading && !condition) || (condition && (bool)globalRegisters["cond"] == true))
			{
				if (disassemble) disassemblyFile.StoreLine(disassembly[currOp]);
				scanpoint = opcodes[currOp].Execute(this, scanpoint, false, runningProcess, globalRegisters);
			}
			else if (reading)
			{
				if (currOp == OPCODE_READEND && accessor.ReadUInt32(scanpoint + difference) == OPCODE_READEND && accessor.ReadUInt32(scanpoint + difference * 2) == OPCODE_READEND && readingCounter == 1)
				{
					if (disassemble) disassemblyFile.StoreLine(disassembly[currOp]);
					readingCounter = 0;
					reading = false;
				}
				else
				{
					if (disassemble) disassemblyFile.StoreLine(currOp.ToString());
					if (currOp < 255)
						runningString = runningString + (char)currOp;
					runningOpcodes.Add(currOp);
					if (currOp == OPCODE_PUSHF || currOp == OPCODE_PUSHB)
					{
						runningOpcodes.Add(accessor.ReadSingle(scanpoint + difference));
						scanpoint += difference;
					}
					if (currOp == OPCODE_READEND && accessor.ReadUInt32(scanpoint + difference) == OPCODE_READEND && accessor.ReadUInt32(scanpoint + difference * 2) == OPCODE_READEND)
						readingCounter -= 1;
				}
			}
			else if (condition)
			{

			}
			scanpoint += difference;
		}
		GD.Print("finished");
		if (export)                                                                         // Export box is checked, take everything we read
		{                                                                                   // and export to SmalltalkBytecode.stb
			var byteExport = new List<byte>();
			var nullCounter = 0;
			for (int i = 0; i < 63000; i++)
			{
				byteExport.Add(accessor.ReadByte(i));
				if (accessor.ReadByte(i) == (byte)0)
				{
					nullCounter += 1;
					if (nullCounter >= 10)
						break;
				}
				else
				{
					nullCounter = 0;
				}
			}
			GD.Print("exported");
			System.IO.File.WriteAllBytes("SmalltalkBytecode.stb", byteExport.ToArray());
		}
		difference = 1;
		globalRoutine = runningProcess;
		oldRoutine = runningProcess;
		routineLoop = true;
		if (disassemble) disassemblyFile.Close();
	}

	public void Interpret()                                             // Same as compute, but we're operating on a bytearray from a file
	{                                                                   // instead of reading the bytecode directly from RAM
		var file = new Godot.File();                                    // Interpret mode is checked, so read in the file variable as a giant string
		file.Open(bytecodeFile, Godot.File.ModeFlags.Read);             // Read in every byte from the bytecode. The default STB size is 2000 bytes, but
		for (int q = 0; q < file.GetLen() - 1; q++)                     // I just keep a 100 byte buffer as a lazy way to ensure we don't go out of bounds
		{
			uint curr32 = file.Get32();
			globalRoutine.Add(curr32);
			if (curr32 == OPCODE_PUSHF || curr32 == OPCODE_PUSHB)
				globalRoutine.Add(file.GetFloat());
			if (q > 63000) break;
		}
		file.Close();                                                   // The file is in memory now, so close it.
		if (globalRoutine.Count == 0) return;
		var reading = false;
		var readingCounter = 0;
		var condition = false;
		var conditionCounter = 0;
		int lastScanpoint = 1;
		int scanpoint = 1;
		uint currOp;
		difference = 1;
		while (scanpoint < globalRoutine.Count)
		{
			currOp = (uint)globalRoutine[scanpoint];

			if (currOp == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference] == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference * 2] == OPCODE_READBEGIN && readingCounter == 0 && !condition ||
				currOp == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference] == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference * 2] == OPCODE_READBEGIN && readingCounter == 0 && (bool)globalRegisters["cond"] == true)
			{
				reading = true;
				readingCounter += 1;
				runningString = "";
				runningOpcodes = new ArrayList();
				scanpoint += difference * 3;
				currOp = (uint)globalRoutine[scanpoint];
			}
			if (reading && currOp == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference] == OPCODE_READBEGIN && (uint)globalRoutine[scanpoint + difference * 2] == OPCODE_READBEGIN)
				readingCounter += 1;
			lastScanpoint = scanpoint;
			if ((!reading && !condition) || (condition && (bool)globalRegisters["cond"] == true))
			{
				scanpoint = opcodes[currOp].Execute(this, scanpoint, true, globalRoutine, globalRegisters);
			}
			else if (reading)
			{
				if (currOp == OPCODE_READEND && (uint)globalRoutine[scanpoint + difference] == OPCODE_READEND && (uint)globalRoutine[scanpoint + difference * 2] == OPCODE_READEND && readingCounter == 1)
				{
					readingCounter = 0;
					reading = false;
				}
				else
				{
					if (currOp < 255)
						runningString = runningString + (char)currOp;
					runningOpcodes.Add(currOp);
					if (currOp == OPCODE_PUSHF || currOp == OPCODE_PUSHB)
					{
						runningOpcodes.Add((float)globalRoutine[scanpoint + difference]);
						scanpoint += difference;
					}
					if (currOp == OPCODE_READEND && (uint)globalRoutine[scanpoint + difference] == OPCODE_READEND && (uint)globalRoutine[scanpoint + difference * 2] == OPCODE_READEND)
						readingCounter -= 1;
				}
			}
			else if (condition)
			{

			}
			scanpoint += difference;
		}
		globalRoutine = runningProcess;
		oldRoutine = runningProcess;
		//for (int y = 0; y < routine.Count; y++) GD.Print(routine[y]);
		routineLoop = true;
	}

	public void ComputeRoutine(SmalltalkVM vm, ArrayList routine, Hashtable registers)                              // This is the version of Interpret/Compute that operates every frame
	{                                                                           // It doesn't read the PROC opcodes to prevent infinite loop errors if
		var reading = false;
		var readingCounter = 0;
		registers["cond"] = true;
		int lastScanpoint = 0;
		int scanpoint = 0;
		uint currOp;
		difference = 1;
		while (scanpoint < routine.Count)
		{
			try
			{
				currOp = (uint)routine[scanpoint];
			}
			catch
			{
				GD.Print("Not an opcode");
				GD.Print(routine[scanpoint]);
				scanpoint += difference;
				break;
			}

			if (currOp == OPCODE_PUSHPROC)
			{
				scanpoint += difference;
				GD.Print("Skipped infinite loop! There's a PUSHPROC opcode in the routine! This shouldn't happen!");
			}

			if (currOp == OPCODE_READBEGIN && (uint)routine[scanpoint + difference] == OPCODE_READBEGIN && (uint)routine[scanpoint + difference * 2] == OPCODE_READBEGIN && readingCounter == 0 && conditionKey == 0)
			{
				reading = true;
				readingCounter += 1;
				runningString = "";
				runningOpcodes = new ArrayList();
				scanpoint += difference * 3;
				currOp = (uint)routine[scanpoint];
			}
			if (reading && currOp == OPCODE_READBEGIN && (uint)routine[scanpoint + difference] == OPCODE_READBEGIN && (uint)routine[scanpoint + difference * 2] == OPCODE_READBEGIN)
				readingCounter += 1;
			lastScanpoint = scanpoint;
			if (!reading)
			{
				if (conditionKey == 0 || currOp == OPCODE_CONDEND)
				{
					scanpoint = opcodes[currOp].Execute(this, scanpoint, true, routine, registers);
				}
				else if (currOp == OPCODE_PUSHF || currOp == OPCODE_PUSHB)
					scanpoint += difference;
			}
			else if (reading)
			{
				if (currOp == OPCODE_READEND && (uint)routine[scanpoint + difference] == OPCODE_READEND && (uint)routine[scanpoint + difference * 2] == OPCODE_READEND && readingCounter == 1)
				{
					readingCounter = 0;
					reading = false;
				}
				else
				{
					if (currOp < 255)
						runningString = runningString + (char)currOp;
					runningOpcodes.Add(currOp);
					if (currOp == OPCODE_PUSHF || currOp == OPCODE_PUSHB)
					{
						runningOpcodes.Add((float)routine[scanpoint + difference]);
						scanpoint += difference;
					}
					if (currOp == OPCODE_READEND && (uint)routine[scanpoint + difference] == OPCODE_READEND && (uint)routine[scanpoint + difference * 2] == OPCODE_READEND)
						readingCounter -= 1;
				}
			}
			scanpoint += difference;
		}
	}


	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _PhysicsProcess(float delta)
	{
		deltaTime = delta;                                                          // We set delta time as a global so our opcodes can access it

		if (!interpret)                                                             // We're not interpreting, so check if we need to compute
		{
			try
			{
				if (accessor.ReadInt32(0) != 0)                                         // The first 4 bytes are a short that indicate to jump to an
				{                                                                       // address.
					var jump = accessor.ReadInt32(0);                                   // It's non-null, so jump to the address
					accessor.Write(0, (short)0);                                        // and replace it with a null so we don't repeat the sequence
					Compute(jump);
				}
			}
			catch
			{

			}
		}
		if (routineLoop)
		{
			//if (debug) for(int i = 0; i < routine.Count; i++) GD.Print(routine[i]);
			ComputeRoutine(this, globalRoutine, globalRegisters);
		}

	}

	public override void _Notification(int what)                                        // On app close, make sure we free the memory if we're
	{                                                                                   // not in interpret mode.
		if (what == MainLoop.NotificationWmQuitRequest)
		{
			try
			{
				accessor.Dispose();
				mmf.Dispose();
				GetTree().Quit(); // default behavior
			}
			catch
			{
				GetTree().Quit();
			}
		}
	}

	public void princessTouchStart(KinematicBody body)
	{
		if (body.Name == "PlayerKB")
			ComputeRoutine(this, (ArrayList)events["princessTouchStart"], globalRegisters);
	}

	public void traveledMainRoads(KinematicBody body)
	{
		if (body.Name == "PlayerKB")
			ComputeRoutine(this, (ArrayList)events["traveledMainRoads"], globalRegisters);
	}

	public void princessTouchEnd(KinematicBody body)
	{
		if (body.Name == "PlayerKB")
			ComputeRoutine(this, (ArrayList)events["princessTouchEnd"], globalRegisters);
	}

	public void GloombaTouched(KinematicBody body)
	{
		if (body.Name == "PrincessKB")
			ComputeRoutine(this, (ArrayList)events["PrincessDed"], globalRegisters);
	}

	public void plumberTouched(KinematicBody body)
	{
		if (body.Name == "PrincessKB" || body.Name == "PlayerKB")
		{
			this.GetTree().ChangeScene("NotMyProblemScenes/win.tscn");
		}
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);

		if (@event is InputEventMouseButton)
		{
			InputEventMouseButton clickEvent = @event as InputEventMouseButton;
			if (events["lc"] != null && clickEvent.ButtonIndex == 1)
			{
				ComputeRoutine(this, (ArrayList)events["lc"], globalRegisters);
			}
		}

		if (@event is InputEventMouseMotion)
		{

			if (events["mm"] != null)
			{
				routineLoop = false;
				InputEventMouseMotion mouseEvent = @event as InputEventMouseMotion;
				mouseRelX = (float)mouseEvent.Relative.x;
				mouseRelY = (float)mouseEvent.Relative.y;

				ComputeRoutine(this, (ArrayList)events["mm"], globalRegisters);
				routineLoop = true;
			}
		}
	}

	public abstract class Opcode : object                                                       // Our baseclass for opcodes. All opcodes are simply
	{                                                                                           // a class with a single function: execute
		public abstract int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers);
	}
	public class Op_Null : Opcode                                                               // Just in case the VM tries to execute a null, do nothing.
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			return scanpoint;
		}
	}

	public uint ReadMemory(int scanpoint, bool read, ArrayList routine, Hashtable registers)
	{
		if (!read)
		{
			return accessor.ReadUInt32(scanpoint);
		}
		else
		{
			return (uint)routine[scanpoint];
		}
	}

	public float ReadMemoryF(int scanpoint, bool read, ArrayList routine, Hashtable registers)
	{
		if (!read)
		{
			return accessor.ReadSingle(scanpoint);
		}
		else
		{
			return (float)routine[scanpoint];
		}
	}
	public class Op_PushBool : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			if ((vm.ReadMemoryF(scanpoint + vm.difference, read, routine, registers)) == 1.0)
				registers[vm.stackTop] = true;
			else
				registers[vm.stackTop] = false;
			vm.stackTop += 1;
			return scanpoint + vm.difference;
		}
	}

	public class Op_PushInteger : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = (uint)(vm.ReadMemory(scanpoint + vm.difference, read, routine, registers));
			vm.stackTop += 1;
			return scanpoint + vm.difference;
		}
	}

	public class Op_PullInteger : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = vm.symbolTable[(uint)(vm.ReadMemory(scanpoint + vm.difference, read, routine, registers))];
			vm.stackTop += 1;
			return scanpoint + vm.difference;
		}
	}

	public class Op_PushFloat : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = (vm.ReadMemoryF(scanpoint + vm.difference, read, routine, registers));
			vm.stackTop += 1;
			return scanpoint + vm.difference;
		}
	}

	public class Op_PushString : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = vm.runningString;
			vm.stackTop += 1;
			return scanpoint;
		}
	}

	public class Op_PushProcess : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			for (int i = 0; i < vm.runningOpcodes.Count; i++)
				vm.runningProcess.Add(vm.runningOpcodes[i]);
			return scanpoint;
		}
	}

	public class Op_AssignSymbol : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			vm.symbolTable[registers[vm.stackTop - 1]] = registers[vm.stackTop - 2];
			vm.stackTop -= 2;
			return scanpoint;
		}
	}
	public class Op_SetCondition : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers["cond"] = registers[vm.stackTop - 1];
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_ConditionBegin : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			if ((bool)registers["cond"] == false)
			{
				vm.conditionKey = (uint)(vm.ReadMemory(scanpoint + vm.difference, read, routine, registers));
			}
			return scanpoint + vm.difference;
		}
	}

	public class Op_ConditionEnd : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			if ((bool)registers["cond"] == false)
			{
				if (vm.conditionKey == (uint)(vm.ReadMemory(scanpoint + vm.difference, read, routine, registers)))
				{
					registers["cond"] = true;
					vm.conditionKey = 0;
				}
			}
			return scanpoint + vm.difference;
		}
	}

	public class Op_Print : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			GD.Print(registers[vm.stackTop - 1]);
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_MoveAndSlide : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers["msresult"] = ((KinematicBody)registers[vm.stackTop - 1]).MoveAndSlide((Vector3)registers[vm.stackTop - 2], vm.upDir);
				vm.stackTop -= 2;
			}
			catch
			{
				Vector3 temp = new Vector3((float)registers[vm.stackTop - 2], (float)registers[vm.stackTop - 3], (float)registers[vm.stackTop - 4]);
				registers["msresult"] = ((KinematicBody)registers[vm.stackTop - 1]).MoveAndSlide(temp, vm.upDir);
				vm.stackTop -= 4;
			}
			return scanpoint;
		}
	}

	public class Op_PushMoveSlideResult : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = registers["msresult"];
			vm.stackTop += 1;
			return scanpoint;
		}
	}

	public class Op_IsOnFloor : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((KinematicBody)(registers[vm.stackTop - 1])).IsOnFloor();
			return scanpoint;
		}
	}

	public class Op_GetNode : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = vm.GetNode((string)registers[vm.stackTop - 1]);
			return scanpoint;
		}
	}

	public class Op_DeltaTime : Opcode                                                // Get Transform from R1, hold basis X
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = vm.deltaTime;
			vm.stackTop += 1;
			return scanpoint;
		}
	}

	public class Op_IsPressed : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			string temp = (string)registers[vm.stackTop - 1];
			registers[vm.stackTop - 1] = Input.IsActionPressed(temp);
			return scanpoint;
		}
	}

	public class Op_JustPressed : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			string temp = (string)registers[vm.stackTop - 1];
			registers[vm.stackTop - 1] = Input.IsActionJustPressed(temp);
			return scanpoint;
		}
	}

	public class Op_PlayAudioStream : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				((AudioStreamPlayer)registers[vm.stackTop - 1]).Play();
			}
			catch
			{
				((AudioStreamPlayer3D)registers[vm.stackTop - 1]).Play();
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_StopAudioStream : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				((AudioStreamPlayer)registers[vm.stackTop - 1]).Stop();
			}
			catch
			{
				((AudioStreamPlayer3D)registers[vm.stackTop - 1]).Stop();
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_SetAudioStreamVolume : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				((AudioStreamPlayer)registers[vm.stackTop - 2]).VolumeDb = vm.stackTop - 1;
			}
			catch
			{
				((AudioStreamPlayer3D)registers[vm.stackTop - 2]).MaxDb = vm.stackTop - 1;
			}
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	// MATH OPCODES **********************************************************************************************************************
	public class Op_Add : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] + (float)registers[vm.stackTop - 2];
			}
			catch
			{
				registers[vm.stackTop - 2] = (Vector3)registers[vm.stackTop - 1] + (Vector3)registers[vm.stackTop - 2];
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_Subtract : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] - (float)registers[vm.stackTop - 2];
			}
			catch
			{
				registers[vm.stackTop - 2] = (Vector3)registers[vm.stackTop - 1] - (Vector3)registers[vm.stackTop - 2];
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}
	public class Op_Multiply : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] * (float)registers[vm.stackTop - 2];
			}
			catch
			{
				try
				{
					registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] * (Vector3)registers[vm.stackTop - 2];
				}
				catch
				{
					registers[vm.stackTop - 2] = (Vector3)registers[vm.stackTop - 1] * (float)registers[vm.stackTop - 2];
				}
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}


	public class Op_Divide : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] / (float)registers[vm.stackTop - 2];
			}
			catch
			{
				try
				{
					registers[vm.stackTop - 2] = (Vector3)registers[vm.stackTop - 2] / (float)registers[vm.stackTop - 1];
				}
				catch
				{
					registers[vm.stackTop - 2] = (Vector3)registers[vm.stackTop - 1] / (float)registers[vm.stackTop - 2];
				}
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}
	public class Op_Negate : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 1] = -((float)registers[vm.stackTop - 1]);
			}
			catch
			{
				try
				{
					registers[vm.stackTop - 1] = -((Vector2)registers[vm.stackTop - 1]);
				}
				catch
				{
					registers[vm.stackTop - 1] = -((Vector3)registers[vm.stackTop - 1]);
				}
			}
			return scanpoint;

		}
	}

	public class Op_GreaterThan : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] > (float)registers[vm.stackTop - 2];
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_GreaterThanEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] >= (float)registers[vm.stackTop - 2];
			vm.stackTop -= 1;
			return scanpoint;
		}
	}
	public class Op_LessThan : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] < (float)registers[vm.stackTop - 2];
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_LessThanEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 2] = (float)registers[vm.stackTop - 1] <= (float)registers[vm.stackTop - 2];
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_Equal : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				if (registers[vm.stackTop - 1].GetType() == typeof(bool))
					registers[vm.stackTop - 2] = ((bool)registers[vm.stackTop - 1] == (bool)registers[vm.stackTop - 2]);
				else if (registers[vm.stackTop - 1].GetType() == typeof(float))
					registers[vm.stackTop - 2] = ((float)registers[vm.stackTop - 1] == (float)registers[vm.stackTop - 2]);
				else if (registers[vm.stackTop - 1].GetType() == typeof(string))
					registers[vm.stackTop - 2] = ((string)registers[vm.stackTop - 1] == (string)registers[vm.stackTop - 2]);
			}
			catch
			{
				registers[vm.stackTop - 2] = (registers[vm.stackTop - 1] == registers[vm.stackTop - 2]);
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_NotEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				if (registers[vm.stackTop - 1].GetType() == typeof(bool))
					registers[vm.stackTop - 2] = ((bool)registers[vm.stackTop - 1] != (bool)registers[vm.stackTop - 2]);
				else if (registers[vm.stackTop - 1].GetType() == typeof(float))
					registers[vm.stackTop - 2] = ((float)registers[vm.stackTop - 1] != (float)registers[vm.stackTop - 2]);
			}
			catch
			{
				registers[vm.stackTop - 2] = (registers[vm.stackTop - 1] != registers[vm.stackTop - 2]);
			}
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_Not : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = !((Boolean)(registers[vm.stackTop - 1]));
			return scanpoint;
		}
	}

	public class Op_SetVector2X : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector2 temp;
			if (vm.symbolTable[registers[vm.stackTop - 1]] != null)
				temp = (Vector2)vm.symbolTable[registers[vm.stackTop - 1]];
			else
				temp = new Vector2();
			temp.x = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetVector2Y : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector2 temp;
			if (vm.symbolTable[registers[vm.stackTop - 1]] != null)
				temp = (Vector2)vm.symbolTable[registers[vm.stackTop - 1]];
			else
				temp = new Vector2();
			temp.y = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetVector2XY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector2 temp = new Vector2();
			temp.y = (float)registers[vm.stackTop - 3];
			temp.x = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 3;
			return scanpoint;
		}
	}

	public class Op_PushVector2X : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Vector2)registers[vm.stackTop - 1]).x;
			return scanpoint;
		}
	}

	public class Op_PushVector2Y : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Vector2)registers[vm.stackTop - 1]).y;
			return scanpoint;
		}
	}

	public class Op_SetVector3X : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector3 temp;
			if (vm.symbolTable[registers[vm.stackTop - 1]] != null)
				temp = (Vector3)vm.symbolTable[registers[vm.stackTop - 1]];
			else
				temp = new Vector3();
			temp.x = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 2;
			return scanpoint;

		}
	}

	public class Op_SetVector3Y : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector3 temp;
			if (vm.symbolTable[registers[vm.stackTop - 1]] != null)
				temp = (Vector3)vm.symbolTable[registers[vm.stackTop - 1]];
			else
				temp = new Vector3();
			temp.y = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetVector3Z : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector3 temp;
			if (vm.symbolTable[registers[vm.stackTop - 1]] != null)
				temp = (Vector3)vm.symbolTable[registers[vm.stackTop - 1]];
			else
				temp = new Vector3();
			temp.z = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetVector3XYZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Vector3 temp = new Vector3();
			temp.z = (float)registers[vm.stackTop - 4];
			temp.y = (float)registers[vm.stackTop - 3];
			temp.x = (float)registers[vm.stackTop - 2];
			vm.symbolTable[registers[vm.stackTop - 1]] = temp;
			vm.stackTop -= 4;
			return scanpoint;
		}
	}

	public class Op_PushVector3X : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Vector3)registers[vm.stackTop - 1]).x;
			return scanpoint;
		}
	}

	public class Op_PushVector3Y : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Vector3)registers[vm.stackTop - 1]).y;
			return scanpoint;
		}
	}

	public class Op_PushVector3Z : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Vector3)registers[vm.stackTop - 1]).z;
			return scanpoint;
		}
	}

	public class Op_DotProduct : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 2] = ((Vector3)registers[vm.stackTop - 1]).Dot((Vector3)registers[vm.stackTop - 2]);
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_LinearInterpolate : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 3] =
				((Vector3)registers[vm.stackTop - 1]).LinearInterpolate((Vector3)registers[vm.stackTop - 2],
				(float)registers[vm.stackTop - 3]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_NormalizeVector : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				registers[vm.stackTop - 1] = ((Vector2)registers[vm.stackTop - 1]).Normalized();
			}
			catch
			{
				registers[vm.stackTop - 1] = ((Vector3)registers[vm.stackTop - 1]).Normalized();
			}
			return scanpoint;
		}
	}

	public class Op_PushGlobalTransform : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).GlobalTransform;
			return scanpoint;
		}
	}

	public class Op_PushBasisX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Transform)registers[vm.stackTop - 1]).basis.x;
			return scanpoint;
		}
	}

	public class Op_PushBasisY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Transform)registers[vm.stackTop - 1]).basis.y;
			return scanpoint;
		}
	}

	public class Op_PushBasisZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Transform)registers[vm.stackTop - 1]).basis.z;
			return scanpoint;
		}
	}

	public class Op_RotateX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).RotateX((float)registers[vm.stackTop - 2]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_RotateY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).RotateY((float)registers[vm.stackTop - 2]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_RotateZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).RotateZ((float)registers[vm.stackTop - 2]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_DegreesToRadians : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = (float)registers[vm.stackTop - 1] * (3.1415f / 180.0f);
			//registers[vm.stackTop - 1] = Mathf.Deg2Rad((float)registers[vm.stackTop - 1]);
			return scanpoint;
		}
	}

	public class Op_Clamp : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 3] = Mathf.Clamp((float)registers[vm.stackTop - 1], (float)registers[vm.stackTop - 2], (float)registers[vm.stackTop - 3]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_StringToEvent : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			vm.events[registers[vm.stackTop - 1]] = vm.runningOpcodes;
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

	public class Op_PushMouseRelativeX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = (float)vm.mouseRelX;
			vm.stackTop += 1;
			return scanpoint;
		}
	}

	public class Op_PushMouseRelativeY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop] = (float)vm.mouseRelY;
			vm.stackTop += 1;
			return scanpoint;
		}
	}

	public class Op_SetMouseModeCaptured : Opcode                                   // Use node path string from register 1 to put scene node into
	{                                                                               // hold register
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Input.SetMouseMode(Input.MouseMode.Captured);
			return scanpoint;
		}
	}
	public class Op_SetMouseModeVisible : Opcode                                    // Use node path string from register 1 to put scene node into
	{                                                                               // hold register
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Input.SetMouseMode(Input.MouseMode.Visible);
			return scanpoint;
		}
	}
	public class Op_SetMouseModeHidden : Opcode                                     // Use node path string from register 1 to put scene node into
	{                                                                               // hold register
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Input.SetMouseMode(Input.MouseMode.Hidden);
			return scanpoint;
		}
	}
	public class Op_SetMouseModeConfined : Opcode                                   // Use node path string from register 1 to put scene node into
	{                                                                               // hold register
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Input.SetMouseMode(Input.MouseMode.Confined);
			return scanpoint;
		}
	}

	public class Op_SetAnimationTreeParameter : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((AnimationTree)registers[vm.stackTop - 1]).Set((string)registers[vm.stackTop - 2], (float)registers[vm.stackTop - 3]);
			vm.stackTop -= 3;
			return scanpoint;
		}
	}

	public class Op_SetAnimationTreeParameterTrue : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((AnimationTree)registers[vm.stackTop - 1]).Set((string)registers[vm.stackTop - 2], true);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetAnimationTreeParameterFalse : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((AnimationTree)registers[vm.stackTop - 1]).Set((string)registers[vm.stackTop - 2], false);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_GetCurrentAnimationPlayer : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((AnimationNodeStateMachinePlayback)registers[vm.stackTop - 1]).GetCurrentNode();
			return scanpoint;
		}
	}
	public class Op_GetPlayback : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((AnimationTree)registers[vm.stackTop - 1]).Get("parameters/playback");
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke0 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			var temp = (caller.Call(method));
			if (temp != null)
			{
				registers[vm.stackTop - 2] = temp;
				vm.stackTop -= 1;
			}
			else
			{
				vm.stackTop -= 2;
			}
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke1 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			object[] args = { registers[vm.stackTop - 3] };
			var temp = (caller.Call(method, args));
			if (temp != null)
			{
				registers[vm.stackTop - 3] = temp;
				vm.stackTop -= 2;
			}
			else
			{
				vm.stackTop -= 3;
			}
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke2 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			object[] args = { registers[vm.stackTop - 3], registers[vm.stackTop - 4] };
			var temp = (caller.Call(method, args));
			if (temp != null)
			{
				registers[vm.stackTop - 4] = temp;
				vm.stackTop -= 3;
			}
			else
			{
				vm.stackTop -= 4;
			}
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke3 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			object[] args = { registers[vm.stackTop - 3], registers[vm.stackTop - 4], registers[vm.stackTop - 5] };
			var temp = (caller.Call(method, args));
			if (temp != null)
			{
				registers[vm.stackTop - 5] = temp;
				vm.stackTop -= 4;
			}
			else
			{
				vm.stackTop -= 5;
			}
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke4 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			object[] args = { registers[vm.stackTop - 3], registers[vm.stackTop - 4], registers[vm.stackTop - 5], registers[vm.stackTop - 6] };
			var temp = (caller.Call(method, args));
			if (temp != null)
			{
				registers[vm.stackTop - 6] = temp;
				vm.stackTop -= 5;
			}
			else
			{
				vm.stackTop -= 6;
			}
			return scanpoint;
		}
	}

	public class Op_CSharpInvoke5 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			Godot.Object caller = (Godot.Object)registers[vm.stackTop - 1];
			string method = (string)registers[vm.stackTop - 2];
			object[] args = { registers[vm.stackTop - 3], registers[vm.stackTop - 4], registers[vm.stackTop - 5], registers[vm.stackTop - 6], registers[vm.stackTop - 7] };
			var temp = (caller.Call(method, args));
			if (temp != null)
			{
				registers[vm.stackTop - 7] = temp;
				vm.stackTop -= 6;
			}
			else
			{
				vm.stackTop -= 7;
			}
			return scanpoint;
		}
	}

	public class Op_PushPositionX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Translation.x;
			return scanpoint;
		}
	}

	public class Op_PushPositionY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Translation.y;
			return scanpoint;
		}
	}

	public class Op_PushPositionZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Translation.z;
			return scanpoint;
		}
	}


	public class Op_PushPosition : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Translation;
			return scanpoint;
		}
	}

	public class Op_SetPosition : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Translation = (Vector3)registers[vm.stackTop - 2];
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetPositionX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Translation = new Vector3((float)registers[vm.stackTop - 2], ((Spatial)registers[vm.stackTop - 1]).Translation.y, ((Spatial)registers[vm.stackTop - 1]).Translation.z);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetPositionY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Translation = new Vector3(((Spatial)registers[vm.stackTop - 1]).Translation.x, (float)registers[vm.stackTop - 2], ((Spatial)registers[vm.stackTop - 1]).Translation.z);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetPositionZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Translation = new Vector3(((Spatial)registers[vm.stackTop - 1]).Translation.x, ((Spatial)registers[vm.stackTop - 1]).Translation.y, (float)registers[vm.stackTop - 2]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetVisible : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			try
			{
				((Spatial)registers[vm.stackTop - 1]).Visible = (bool)registers[vm.stackTop - 2];
			}
			catch
			{
				((Control)registers[vm.stackTop - 1]).Visible = (bool)registers[vm.stackTop - 2];
			}
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_PushRotationX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Rotation.x;
			return scanpoint;
		}
	}

	public class Op_PushRotationY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Rotation.y;
			return scanpoint;
		}
	}

	public class Op_PushRotationZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Rotation.z;
			return scanpoint;
		}
	}


	public class Op_PushRotation : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			registers[vm.stackTop - 1] = ((Spatial)registers[vm.stackTop - 1]).Rotation;
			return scanpoint;
		}
	}

	public class Op_SetRotation : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Rotation = (Vector3)registers[vm.stackTop - 2];
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetRotationX : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Rotation = new Vector3((float)registers[vm.stackTop - 2], ((Spatial)registers[vm.stackTop - 1]).Rotation.y, ((Spatial)registers[vm.stackTop - 1]).Rotation.z);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetRotationY : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Rotation = new Vector3(((Spatial)registers[vm.stackTop - 1]).Rotation.x, (float)registers[vm.stackTop - 2], ((Spatial)registers[vm.stackTop - 1]).Rotation.z);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_SetRotationZ : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			((Spatial)registers[vm.stackTop - 1]).Rotation = new Vector3(((Spatial)registers[vm.stackTop - 1]).Rotation.x, ((Spatial)registers[vm.stackTop - 1]).Rotation.y, (float)registers[vm.stackTop - 2]);
			vm.stackTop -= 2;
			return scanpoint;
		}
	}

	public class Op_ChangeScene : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read, ArrayList routine, Hashtable registers)
		{
			vm.GetTree().ChangeScene((string)registers[vm.stackTop - 1]);
			vm.stackTop -= 1;
			return scanpoint;
		}
	}

}
